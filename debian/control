Source: ansible-core
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Lee Garrett <debian@rocketjump.eu>
Section: admin
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3 (>= 3.9.0~),
#               python3-antsibull <!nodoc>,
               python3-docutils,
               python3-jinja2 (>= 3.0.0~),
               python3-packaging,
#               python3-pygments <!nodoc>,
               python3-resolvelib (>= 0.5.3~),
               python3-resolvelib (<< 1.2.0),
#               python3-rstcheck <!nodoc>,
               python3-setuptools,
               python3-straight.plugin,
#               python3-sphinx <!nodoc>,
#               python3-sphinx-notfound-page <!nodoc>,
#               python3-sphinx-rtd-theme <!nodoc>,
               python3-yaml (>= 5.1~)
#               sphinx-common
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/ansible-core
Vcs-Git: https://salsa.debian.org/python-team/packages/ansible-core.git
Homepage: https://www.ansible.com

Package: ansible-core
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         openssh-client | python3-paramiko (>= 2.6.0),
         python3-dnspython,
         python3-httplib2,
         python3-jinja2 (>= 3.0.0~),
         python3-netaddr,
         python3-yaml (>= 5.1~)
Recommends: ansible (>= 9.5.1-1~),
            python3-argcomplete,
            python3-cryptography,
            python3-jmespath,
            python3-kerberos,
            python3-libcloud,
            python3-passlib,
            python3-selinux,
            python3-winrm,
            python3-xmltodict
Suggests: cowsay,
          sshpass
Breaks: ansible (<< 4.6.0-1~), ansible-base
Replaces: ansible (<< 4.6.0-1~), ansible-base
Description: Configuration management, deployment, and task execution system
 Ansible is a radically simple model-driven configuration management,
 multi-node deployment, and remote task execution system. Ansible works
 over SSH and does not require any software or daemons to be installed
 on remote nodes. Extension modules can be written in any language and
 are transferred to managed machines automatically.
 .
 This package contains the ansible binaries.

#Package: ansible-doc
#Architecture: all
#Section: doc
#Depends: ${misc:Depends},
#         ${sphinxdoc:Depends},
#         fonts-font-awesome,
#         fonts-roboto-fontface,
#         libjs-jquery,
#         libjs-modernizr,
#         libjs-underscore
#Description: Ansible documentation and examples
# Ansible is a radically simple model-driven configuration management,
# multi-node deployment, and remote task execution system. Ansible works
# over SSH and does not require any software or daemons to be installed
# on remote nodes. Extension modules can be written in any language and
# are transferred to managed machines automatically.
# .
# This package contains HTML documentation and examples.
#Build-Profiles: <!nodoc>
