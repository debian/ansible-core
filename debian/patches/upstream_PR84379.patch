Description: Fix integration tests on debian
 Couple of fixes to make the integration tests work again on Debian. Currently
 ansible project CI tests don't run against Debian, but Debian downstream does.
Author: Lee Garrett <debian@rocketjump.eu>
Origin: upstream, https://patch-diff.githubusercontent.com/raw/ansible/ansible/pull/84379.patch
Forwarded: https://github.com/ansible/ansible/pull/84379
Last-Update: 2024-12-18
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/

--- a/test/integration/targets/service_facts/tasks/main.yml
+++ b/test/integration/targets/service_facts/tasks/main.yml
@@ -3,10 +3,6 @@
 # Copyright: (c) 2020, Abhijeet Kasurde <akasurde@redhat.com>
 # GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
 
-- name: skip broken distros
-  meta: end_host
-  when: ansible_distribution == 'Alpine'
-
 - name: Gather service facts
   service_facts:
 
@@ -26,4 +22,6 @@
   - name: execute tests
     import_tasks: tests.yml
 
-  when: (ansible_distribution in ['RedHat', 'CentOS', 'ScientificLinux'] and ansible_distribution_major_version is version('7', '>=')) or ansible_distribution == 'Fedora' or (ansible_distribution == 'Ubuntu' and ansible_distribution_version is version('15.04', '>=')) or (ansible_distribution == 'Debian' and ansible_distribution_version is version('8', '>=')) or ansible_os_family == 'Suse'
+  when:
+    - ansible_service_mgr == "systemd"
+    - ansible_distribution != 'Alpine'
--- a/test/integration/targets/interpreter_discovery_python/tasks/main.yml
+++ b/test/integration/targets/interpreter_discovery_python/tasks/main.yml
@@ -140,9 +140,9 @@
 
   - name: debian assertions
     assert:
+      # versioned interpreter gets discovered, ensure it's at least py >= 3
       that:
-      # Debian 10 and newer
-      - auto_out.ansible_facts.discovered_interpreter_python == '/usr/bin/python3' and distro_version is version('10', '>=') or distro_version is version('10', '<')
+        - auto_out.ansible_facts.discovered_interpreter_python|regex_search('^/usr/bin/python3')
     when: distro == 'debian'
 
   - name: fedora assertions
--- a/test/integration/targets/setup_deb_repo/tasks/main.yml
+++ b/test/integration/targets/setup_deb_repo/tasks/main.yml
@@ -61,6 +61,9 @@
       - testing
     when: install_repo|default(True)|bool is true
 
+  when: ansible_distribution in ['Ubuntu', 'Debian']
+
+- block:
   # Need to uncomment the deb-src for the universe component for build-dep state
   - name: Ensure deb-src for the universe component
     lineinfile:
@@ -80,4 +83,4 @@
       sed -i 's/^Types: deb$/Types: deb deb-src/' /etc/apt/sources.list.d/ubuntu.sources
     when: ansible_distribution_version is version('24.04', '>=')
 
-  when: ansible_distribution in ['Ubuntu', 'Debian']
+  when: ansible_distribution == 'Ubuntu'
--- a/test/integration/targets/hostname/tasks/Debian.yml
+++ b/test/integration/targets/hostname/tasks/Debian.yml
@@ -16,5 +16,5 @@
 - name: Test DebianStrategy using assertions
   assert:
     that:
-      - "'{{ ansible_distribution_release }}-bebop.ansible.example.com' in get_hostname.stdout"
-      - "'{{ ansible_distribution_release }}-bebop.ansible.example.com' in grep_hostname.stdout"
+      - "ansible_distribution_release ~ '-bebop.ansible.example.com' in get_hostname.stdout"
+      - "ansible_distribution_release ~ '-bebop.ansible.example.com' in grep_hostname.stdout"
--- a/test/integration/targets/subversion/vars/Debian.yml
+++ b/test/integration/targets/subversion/vars/Debian.yml
@@ -1,5 +1,6 @@
 ---
 subversion_packages:
+- apache2-utils
 - subversion
 - libapache2-mod-svn
 apache_user: www-data
